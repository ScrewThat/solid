package domain;

public class DesignArch implements Outsidestructure {
    public DesignArch() {
    }

    private void designOut() {
        System.out.println("I`m Working on Outside design");
    }

    public void designOutside() {
        this.designOut();
    }

    public void design() {
        this.designOutside();
    }

    public void job() {
        this.design();
    }
}

