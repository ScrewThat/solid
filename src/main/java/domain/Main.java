package domain;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        Company company = new Company();
        company.addEmployee(new DesignArch());
        company.addEmployee(new StructurArch());
        company.addEmployee(new DesignArch());
        company.startJob();
    }
}