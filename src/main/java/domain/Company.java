package domain;

import java.util.ArrayList;
import java.util.List;

public class Company {
    private List<Employee> employees = new ArrayList();

    public Company() {
    }

    public void startJob() {
        this.employees.forEach((e) -> {
            e.job();
        });
    }

    public void addEmployee(Employee employee) {
        this.employees.add(employee);
    }
}