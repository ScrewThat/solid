package domain;

public class StructurArch implements Insidestructure {
    public StructurArch() {
    }

    private void designIn() {
        System.out.println("Im working on inside structure(design)");
    }

    public void designInside() {
        this.designIn();
    }

    public void design() {
        this.designInside();
    }

    public void job() {
        this.design();
    }
}
