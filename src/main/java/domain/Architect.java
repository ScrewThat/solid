package domain;

public interface Architect extends Employee {
    void design();
}