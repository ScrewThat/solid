package domain;

public interface Insidestructure extends Architect {
    void designInside();
}